
# NCHS Robotics Repository

Welcome developers to the North Central Robotics repository. This is a service
for controlling the changes made to source code and backing up your progress as
you work. This is known as version control. 

## There are a few reasons for using this tool.
### 1. You can work remotely
A benefit of this tools is that your source code can be synced between multiple
computers, which means that you can develop on multiple machines seamlessly.

### 2. You can save your progress and revert (should something go wrong)
The version control program that this service uses, "git", allows the user to 
create these things called branchs. Branchs are a "branch" off of your current
progress. After making changes on this branch, you can "merge" it with your the
original source or, if it doesn't work, cast it out all together. The importance
of branches is that it keeps code that works from getting destroyed by new
features.

For our purposes, there will be two branchs. One called "master" and many more
for each feature added to the project. The master branch contains all of the 
source that is used on the robots. This means that everything on the master
branch MUST work. To maintain this, development branchs are to be used to in
order to protect the existing source. To do this, you will branch the master
branch, make your changes, and then submit a merge request to me. I will then
review the changes you have made and either merge it if it works, or help you
fix it so that it can be merged. I will go into details with this later.

Now that I have gone over why this tool is being used, I will talk about how it
is to be used. Here is how this will work:

## Repository Structure
The repository will have a directory for each group. Each directory will be 
named like such: group#_groupname (example group1_UPSBot). All source files for
a project will be contained in the direcotory. Each file will have a naming 
convetion like such: group#_groupname_filename.c (example: group1_UPSBot_maintask.c).

## Workflow
To work on a project, each member will clone into the repository and get to work.
The first step to getting to work is creating a branch to work on. Naming convention
is as such: "group#_couple_word_description_with_under_scores" 
(example: group1_adding_task_main). All source files they make will follow the 
naming convention. As they work, they will commit each feature they add. Each 
commit message should look like this: 
"group#_groupname: brief description of what they added" 
(example: "group1_UPSBot: Added main task"). 
This way, everyone has a clear picture of what is going on, reverting is easy, 
and I can monitor what everyone is doing. Once a feature is ready to commit, they
will push all progress to their branch and submit a merge request. Once they have
sent me a merge request, I will review what they have done. If they write good
code, it will be merged with the master branch. If it is not, then I will work
with them to fix it.

Like I said, I will teach everyone how to use this tool. It can be a bit
confusing at times.

If you have an questions, feel free to email me at chucks.8090@gmail.com